# Docker Builder
> Docker image to build docker projects with batteries included

### Tools included

 - [docker-compose](https://docs.docker.com/compose)
 - [dockerize](https://github.com/jwilder/dockerize)

Suggest more tools openning a [issue](https://gitlab.com/pantacor/internal/dockerbuilder/issues)

### License

See [license](/LICENSE)
